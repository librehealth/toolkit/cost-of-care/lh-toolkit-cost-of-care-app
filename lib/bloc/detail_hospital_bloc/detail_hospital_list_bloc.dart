import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:cost_of_care/bloc/compare_hospital_bloc/compare_hospital_screen/compare_hospital_screen_bloc.dart';
import 'package:cost_of_care/repository/compare_screen_repository_impl.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
part 'detail_hospital_list_event.dart';
part 'detail_hospital_list_state.dart';

class DetailHospitalListBloc
    extends Bloc<DetailHospitalListEvent, DetailHospitalListState> {
  CompareScreenRepositoryImpl compareScreenRepositoryImpl;
  DetailHospitalListBloc(this.compareScreenRepositoryImpl)
      : super(LoadingState());

  List<List<dynamic>> hospitalCompareData;
  List<List<dynamic>> filterhospitals;
  List<List<dynamic>> hospitals = [];
  //int hospitalsAddedToCompare = 0;
  @override
  Stream<DetailHospitalListState> mapEventToState(
    DetailHospitalListEvent event,
  ) async* {
    if (event is GetCompareData) {
      yield LoadingState();
      //hospitalsAddedToCompare = 0;
      try {
        hospitalCompareData =
            await compareScreenRepositoryImpl.getListOfHospitals();
        filterhospitals = hospitalCompareData;
        yield LoadedStateDetail(hospitalCompareData);
      } catch (e) {
        yield ShowSnackBar(e.message);
        yield ErrorState(e.message);
      }
    } else if (event is SearchCompareData) {
      try {
        List<List<dynamic>> copyList = hospitalCompareData;

        // hospitalCompareData = copyList;
        filterhospitals = [];

        copyList.forEach((element) {
          if (element[0].toLowerCase().contains(event.query)) {
            filterhospitals.add(element);
          }
        });
        // hospitalCompareData = filterhospitals;
        // print(filterhospitals.length.toString());
        // print(filterhospitals.toString() + 'my value is');
        if (filterhospitals.isEmpty) {
          yield ErrorState('No Result Found');
        } else {
          yield LoadedStateDetail(filterhospitals);
        }
      } catch (e) {
        yield ErrorState('something wrong');
      }
    } else if (event is UpdateHospitalToCompare) {
      
        filterhospitals[event.index][13] =
            (filterhospitals[event.index][13]) == 0 ? 1 : 0;
        yield LoadedStateDetail(filterhospitals);
      
    } else if (event is FloatingCompareHospitalButtonPress) {
      
        if (hospitalCompareData == null) {
          yield ShowSnackBar(
              "No hospitals available to compare for your location");
          yield ErrorState(
              "No hospitals available to compare for your location");
        }else{
          
        List<List<dynamic>> addHospitals = [];
        hospitalCompareData.forEach((element) {
          if (element[13] == 1) {
            addHospitals.add(element);
          }
        });
        event.compareHospitalScreenBloc.add(AddHospitals(addHospitals));
      
        }

    }
  }
}
