import 'package:cost_of_care/models/download_cdm_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

@immutable
abstract class DownloadCdmState extends Equatable {
  const DownloadCdmState();
}

class LoadingState extends DownloadCdmState {
  @override
  List<Object> get props => [];
}

class AddedToBookmarkState extends DownloadCdmState {
  final int index;

  AddedToBookmarkState(this.index);

  @override
  List<Object> get props => [index];
}

class RemovedToBookmarkState extends DownloadCdmState {
  final int index;

  RemovedToBookmarkState(this.index);

  @override
  List<Object> get props => [index];
}

class LoadedState extends DownloadCdmState {
  final List<DownloadCdmModel> hospitalsName;

  LoadedState(this.hospitalsName);

  @override
  List<Object> get props => [hospitalsName];
}

class RefreshedState extends DownloadCdmState {
  final List<DownloadCdmModel> hospitalsName;
  RefreshedState(this.hospitalsName);

  @override
  List<Object> get props => [hospitalsName];
}

class ErrorStateSnackbar extends DownloadCdmState {
  @override
  List<Object> get props => [];
}

class ErrorState extends DownloadCdmState {
  final String message;

  ErrorState(this.message);

  @override
  List<Object> get props => [message];
}
