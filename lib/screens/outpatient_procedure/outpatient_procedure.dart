import 'package:cost_of_care/screens/outpatient_procedure/components/search_outpatient.dart';
import 'package:flutter/material.dart';

import 'package:cost_of_care/screens/outpatient_procedure/components/body.dart';

class OutpatientProcedureScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Outpatient Procedures'),
        centerTitle: true,
        backgroundColor: Colors.orange,
        leading: BackButton(color: Colors.white),
        actions: [
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              showSearch(
                context: context,
                delegate: SearchOutpatient(),
              );
            },
          )
        ],
      ),
      body: Body(),
    );
  }
}
