
import 'package:cost_of_care/bloc/compare_hospital_bloc/compare_hospital_screen/compare_hospital_screen_bloc.dart';
import 'package:cost_of_care/repository/compare_screen_repository_impl.dart';
import 'package:cost_of_care/screens/compare_hospitals/compare_hospital_screen/components/general_information.dart';
import 'package:cost_of_care/screens/compare_hospitals/compare_hospital_screen/components/patient_survey.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class DetailBody extends StatefulWidget {
  @override
  _DetailBodyState createState() => _DetailBodyState();
}

class _DetailBodyState extends State<DetailBody> {
  CompareScreenRepositoryImpl compareScreenRepositoryImpl =
      new CompareScreenRepositoryImpl();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CompareHospitalScreenBloc, CompareHospitalScreenState>(
        builder: (BuildContext context, CompareHospitalScreenState state) {
      if (state is LoadedState) {
        return SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(8),
              child: Column(children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                SizedBox(
                  height: 5,
                ),
                GeneralInformationWidget(state.hospitalsData),
                PatientSurveyWidget(state.hospitalsData),
              ])),
        );
      }
      return Center(
          child: Container(
        child: CircularProgressIndicator(),
      ));
    });
  }
}
