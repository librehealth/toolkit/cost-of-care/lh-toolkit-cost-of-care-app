import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_list/bloc.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/download_file_button_bloc.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/download_file_button_event.dart';
import 'package:cost_of_care/bloc/download_cdm_bloc/download_cdm_progress/download_file_button_state.dart';
import 'package:cost_of_care/bloc/refresh_saved_cdm_bloc/refresh_saved_cdm_bloc.dart';
import 'package:cost_of_care/models/download_cdm_model.dart';
import 'package:cost_of_care/screens/view_cdm/view_cdm.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:shimmer/shimmer.dart';

ListTile makeShimmerListTile() {
  return ListTile(
    title: Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.white,
      child: Container(
        height: 20,
        decoration: BoxDecoration(
            color: Colors.grey[300],
            borderRadius: BorderRadius.all(Radius.circular(20))),
      ),
    ),
  );
}

Widget makeListTile(
  BuildContext context,
  DownloadCdmModel hospital,
  int index,
  DownloadFileButtonBloc downloadFileButtonBloc,
  String stateName,
  bool bookmark,
) {
  return (bookmark == true)
      ? ((hospital.isBookmarked == 1)
          ? Container(
              height: 120,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  bookmarkedWidget(
                      hospital, index, downloadFileButtonBloc, stateName),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                            hospital.hospitalName ?? '',
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'Source',
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                            hospital.hospitalAddress ?? '',
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.black38,
                              fontFamily: 'Source',
                              fontWeight: FontWeight.w600,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 10),
                    child: downloadWidget(
                        hospital, index, downloadFileButtonBloc, stateName),
                  ),
                ],
              ),
            )
          : Visibility(
              visible: false,
              child: SizedBox(
                height: 0.01,
              ),
            ))
      : Container(
          height: 120,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              bookmarkedWidget(
                  hospital, index, downloadFileButtonBloc, stateName),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Text(
                        hospital.hospitalName ?? '',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Source',
                          fontWeight: FontWeight.w600,
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Text(
                        hospital.hospitalAddress ?? '',
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.black38,
                          fontFamily: 'Source',
                          fontWeight: FontWeight.w600,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 10),
                child: downloadWidget(
                    hospital, index, downloadFileButtonBloc, stateName),
              ),
            ],
          ),
        );
}

Widget bookmarkedWidget(DownloadCdmModel hospital, int index,
    DownloadFileButtonBloc downloadFileButtonBloc, String stateName) {
  return BlocBuilder<DownloadCdmBloc, DownloadCdmState>(
    builder: (context, state) {
      return Material(
        borderRadius: BorderRadius.circular(20.0),
        child: InkWell(
          splashColor: Colors.orange,
          borderRadius: BorderRadius.circular(20.0),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: (hospital.isBookmarked == 1 || state is AddedToBookmarkState)
                ? InkWell(
                    onTap: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          'Removed From bookmark',
                          style: TextStyle(color: Colors.white),
                        ),
                        duration: Duration(milliseconds: 500),
                        backgroundColor: Colors.red,
                      ));
                      BlocProvider.of<DownloadCdmBloc>(context)
                          .add(RemoveToBookmark(index, stateName));
                    },
                    child: Icon(
                      Icons.bookmark_remove,
                      color: Colors.black,
                      size: 40,
                    ),
                  )
                : InkWell(
                    onTap: () {
                      BlocProvider.of<DownloadCdmBloc>(context)
                          .add(AddToBookmark(index, stateName));
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          'Added To bookmark',
                          style: TextStyle(color: Colors.white),
                        ),
                        duration: Duration(milliseconds: 500),
                        backgroundColor: Colors.green,
                      ));
                    },
                    child: Icon(
                      Icons.bookmark_add,
                      color: Colors.orange,
                      size: 40,
                    ),
                  ),
          ),
        ),
      );
    },
  );
}

Widget downloadWidget(DownloadCdmModel hospital, int index,
    DownloadFileButtonBloc downloadFileButtonBloc, String stateName) {
  return BlocBuilder<DownloadFileButtonBloc, DownloadFileButtonState>(
    bloc: downloadFileButtonBloc,
    builder: (BuildContext context, DownloadFileButtonState state) {
      if (state is DownloadButtonLoadingCircular && index == state.index) {
        return CircularProgressIndicator();
      } else if (state is DownloadButtonStream && index == state.index) {
        return StreamBuilder<FileResponse>(
            stream: state.fileStream,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                downloadFileButtonBloc.add(DownloadFileButtonError(
                    "Please check your internet connection and try again"));
                return Material(
                    borderRadius: BorderRadius.circular(20.0),
                    child: InkWell(
                      splashColor: Colors.orange,
                      borderRadius: BorderRadius.circular(20.0),
                      onTap: () async {
                        if ((downloadFileButtonBloc.state
                                is InitialDownloadFileButtonState) ||
                            (downloadFileButtonBloc.state
                                is DownloadButtonErrorState) ||
                            (downloadFileButtonBloc.state
                                is DownloadButtonLoaded)) {
                          downloadFileButtonBloc.add(DownloadFileButtonClick(
                              index,
                              hospital.hospitalName,
                              stateName,
                              downloadFileButtonBloc));
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(
                              'Please wait',
                              style: TextStyle(color: Colors.white),
                            ),
                            backgroundColor: Colors.green,
                          ));
                        }
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.file_download,
                          color: Colors.orange,
                          size: 32,
                        ),
                      ),
                    ));
              } else if (snapshot.connectionState == ConnectionState.active &&
                  snapshot.data is DownloadProgress) {
                double fileSize = state.fileSize;
                DownloadProgress downloaded = (snapshot.data);
                double progress = (downloaded.downloaded / fileSize);
                //showing progress from 60 %
                return progressIndicator(progress * 0.6);
              } else if (snapshot.connectionState == ConnectionState.done) {
                FileInfo fileInfo = snapshot.data as FileInfo;
                downloadFileButtonBloc.add(InsertInDatabase(index, fileInfo,
                    hospital.hospitalName, downloadFileButtonBloc));
                return progressIndicator(0.6);
              } else {
                return CircularProgressIndicator();
              }
            });
      } else if (state is DownloadButtonLoadingProgressIndicator &&
          index == state.index) {
        return progressIndicator(state.progress);
      } else if ((state is DownloadButtonLoaded &&
              index == state.index &&
              hospital.isDownload == 1) ||
          (hospital.isDownload == 1)) {
        return ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.orange,
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => ViewCDM(hospital.hospitalName)),
            );
          },
          child: Text(
            'View',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.white),
          ),
        );
      }
      return Material(
          borderRadius: BorderRadius.circular(20.0),
          child: InkWell(
            splashColor: Colors.orange,
            borderRadius: BorderRadius.circular(20.0),
            onTap: () async {
              if (((downloadFileButtonBloc.state
                          is InitialDownloadFileButtonState) ||
                      (downloadFileButtonBloc.state
                          is DownloadButtonErrorState) || // Make sure CDMs are not getting refreshed at this time
                      (downloadFileButtonBloc.state is DownloadButtonLoaded)) &&
                  !(BlocProvider.of<RefreshSavedCdmBloc>(context).state
                      is RefreshSavedCdmStart)) {
                downloadFileButtonBloc.add(DownloadFileButtonClick(
                    index,
                    hospital.hospitalName,
                    stateName,
                    BlocProvider.of<DownloadFileButtonBloc>(context)));
              } else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                    'Please wait',
                    style: TextStyle(color: Colors.white),
                  ),
                  backgroundColor: Colors.green,
                ));
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                Icons.file_download,
                color: Colors.orange,
                size: 32,
              ),
            ),
          ));
    },
  );
}

Widget progressIndicator(double progress) {
  Color foreground = Colors.red;
  if (progress >= 0.8) {
    foreground = Colors.green;
  } else if (progress >= 0.4) {
    foreground = Colors.orange;
  }
  Color background = foreground.withOpacity(0.2);

  return Stack(
    children: <Widget>[
      SizedBox(
        height: 45.0,
        width: 45.0,
        child: CircularProgressIndicator(
          strokeWidth: 6,
          valueColor: new AlwaysStoppedAnimation<Color>(foreground),
          backgroundColor: background,
          value: progress,
        ),
      ),
      Positioned.fill(
        child: Align(
          alignment: Alignment.center,
          child: Text((progress * 100).toStringAsFixed(0)),
        ),
      )
    ],
  );
}
