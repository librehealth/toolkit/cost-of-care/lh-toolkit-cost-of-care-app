import 'package:cost_of_care/bloc/outpatient_procedure_bloc/outpatient_procedure_bloc.dart';
import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:cost_of_care/repository/inpatient_outpatient_procedure_repository_impl.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:bloc_test/bloc_test.dart';

class MockOutPatientRepository extends Mock
    implements InpatientOutpatientProcedureRepositoryImpl {}

void main() {
  OutpatientProcedureBloc outpatientProcedureBloc;
  MockOutPatientRepository outpatientRepository;
  setUp(() {
    outpatientRepository = MockOutPatientRepository();
    outpatientProcedureBloc = OutpatientProcedureBloc(outpatientRepository);
  });
  tearDown(() {
    outpatientProcedureBloc?.close();
  });

  test('initial state is correct', () {
    expect(outpatientProcedureBloc.initialState, OutpatientProcedureInitial());
  });
  group('Bloc test', () {
    List<OutpatientProcedure> outpatientlist = [];
    outpatientlist.add(OutpatientProcedure('xyz', 1));

    blocTest(
      'emits initialstate  when OutpatientProcedureFetchData() is added',
      build: () => OutpatientProcedureBloc(outpatientRepository),
      act: (bloc) => bloc.add(OutpatientProcedureFetchData()),
      expect: () => [OutpatientProcedureInitial()],
    );
    blocTest(
      'emits lodingstate when getOutpatientProcedures() is added',
      build: () {
        //InpatientProcedureBloc(inpatientRepository);
        when(outpatientRepository.getOutpatientProcedures())
            .thenAnswer((realInvocation) => Future.value(outpatientlist));
        return outpatientProcedureBloc;
      },
      act: (bloc) =>
          bloc.add(OutpatientProcedureFetchData(isHardRefresh: true)),
      expect: () => [
        OutpatientProcedureInitial(),
        OutpatientProcedureLoadingState(),
        OutpatientProcedureLoadedState(outpatientlist)
      ],
    );
    blocTest(
      'emits OutpatientProcedureInitial(), OutpatientProcedureLoadedState( ) when OutpatientProcedureFetchData() is added',
      build: () {
        //InpatientProcedureBloc(inpatientRepository);
        when(outpatientRepository.checkSavedOutpatient())
            .thenAnswer((realInvocation) => true);
        return outpatientProcedureBloc;
      },
      act: (bloc) =>
          bloc.add(OutpatientProcedureFetchData(isHardRefresh: false)),
      expect: () =>
          [OutpatientProcedureInitial(), OutpatientProcedureLoadedState(null)],
    );
  });
}
