import 'package:cost_of_care/bloc/inpatient_procedure_bloc/inpatient_procedure_bloc.dart';
import 'package:cost_of_care/models/outpatient_procedure.dart';
import 'package:cost_of_care/repository/inpatient_outpatient_procedure_repository_impl.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:bloc_test/bloc_test.dart';

class MockInpatientRepository extends Mock
    implements InpatientOutpatientProcedureRepositoryImpl {}

void main() {
  InpatientProcedureBloc inpatientProcedureBloc;
  MockInpatientRepository inpatientRepository;
  setUp(() {
    inpatientRepository = MockInpatientRepository();
    inpatientProcedureBloc = InpatientProcedureBloc(inpatientRepository);
  });
  tearDown(() {
    inpatientProcedureBloc?.close();
  });

  test('initial state is correct', () {
    expect(inpatientProcedureBloc.initialState, InpatientProcedureInitial());
  });
  group('Bloc test', () {
    List<OutpatientProcedure> inpatientlist = [];
    inpatientlist.add(OutpatientProcedure('xyz', 1));

    blocTest(
      'emits initialstate  when FetchData() is added',
      build: () => InpatientProcedureBloc(inpatientRepository),
      act: (bloc) => bloc.add(FetchData()),
      expect: () => [InpatientProcedureInitial()],
    );
    blocTest(
      'emits lodingstate when FetchData() is added',
      build: () {
        //InpatientProcedureBloc(inpatientRepository);
        when(inpatientRepository.getInpatientProcedures())
            .thenAnswer((realInvocation) => Future.value(inpatientlist));
        return inpatientProcedureBloc;
      },
      act: (bloc) => bloc.add(FetchData(isHardRefresh: true)),
      expect: () => [
        InpatientProcedureInitial(),
        InpatientProcedureLoadingState(),
        InpatientProcedureLoadedState(inpatientlist)
      ],
    );
    blocTest(
      'emits lodingstate when FetchData() is added',
      build: () {
        //InpatientProcedureBloc(inpatientRepository);
        when(inpatientRepository.checkSavedInpatient())
            .thenAnswer((realInvocation) => true);
        return inpatientProcedureBloc;
      },
      act: (bloc) => bloc.add(FetchData(isHardRefresh: false)),
      expect: () =>
          [InpatientProcedureInitial(), InpatientProcedureLoadedState(null)],
    );
  });
}
